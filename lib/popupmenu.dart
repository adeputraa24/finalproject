class popup {
  
  static const String AboutUs = 'About Us';
  static const String SignOut = 'Sign Out';
  static const String LompatAyat = 'Lompat Ayat';

  static const List<String> choices = <String>[
    AboutUs,
    SignOut
  ];

  static const List<String> choicesSurah = <String>[
    LompatAyat,
  ];
}
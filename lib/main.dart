import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:myquran/auth/authfirebase.dart';
import 'package:myquran/controllers/surahverse_controller.dart';
import 'package:myquran/homescreen.dart';
import 'package:myquran/splashscreen.dart';
import 'package:myquran/test.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final SurahVerseController svc = Get.put(SurahVerseController());

  //inisialisasi firestore untuk koleksi allsurah
  Query<Map<String, dynamic>> allSurah =
      FirebaseFirestore.instance.collection('allSurah').orderBy('nomer');

  
  //mendapatkan data list surah yang akan digunakan untuk dropdown menu
  allSurah.get().then((data) => data.docs.forEach((doc) {
        svc.listSurahName.add(doc['namaLatin']);
      }));

  runApp(GetMaterialApp(
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    debugShowCheckedModeBanner: false,
    defaultTransition: Transition.fade,
    title: 'MyQuran',
    // home: homeScreen(),
    home: AuthService().onAuthState() == null ? splashScreen() : homeScreen(),
  ));
}

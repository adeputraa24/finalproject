import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class layout {
  final EdgeInsets _margin = const EdgeInsets.only(left: 25, right: 25);
  // final Color _titlecolor = const Color(0xFF64379A);
  final Color _titlecolor = const Color(0xFF194076);
  // final Color _greencolor = const Color(0xFF18E27A);
  final Color _greencolor = const Color(0xFF118448);
  final Color _buttoncolor = const Color(0xFFA313E7);
  final Color _bgcolor = const Color(0xFFFDFCFC);
  final Color _linetabcolor = const Color(0xFF6F40F2);
  final Color _greycolor = const Color(0xFF696969);
  final LinearGradient _lastreadcolor = const LinearGradient(
    
      colors: [Color(0xFFB49CA9), Color(0xFFAC91CF), Color(0xFF8A79F0)]);
  final Color _surahcolor = const Color(0xFF6F40F2);
  final Color _noayatcolor = const Color(0xFF5B33CB);
  final Color _bgayatcolor = const Color(0xFFE3E3E3);
  final LinearGradient _gradayat =
      const LinearGradient(colors: [Color(0xFF00D2FF), Color(0xFF3A7BD5)]);

  EdgeInsets get margin => _margin;
  Color get greencolor => _greencolor;
  Color get titlecolor => _titlecolor;
  Color get btncolor => _buttoncolor;
  Color get bgcolor => _bgcolor;
  Color get linetabcolor => _linetabcolor;
  Color get greycolor => _greycolor;
  LinearGradient get gradread => _lastreadcolor;
  Color get surahcolor => _surahcolor;
  Color get noayatcolor => _noayatcolor;
  Color get bgayatcolor => _bgayatcolor;
  LinearGradient get gradayat => _gradayat;
  
  Widget txtmainfont(String txt,double size, FontWeight fontWeight, Color color) {
    return Text(txt,style: GoogleFonts.martelSans(
        textStyle:
            TextStyle(
              color: color, 
              fontSize: size, 
              fontWeight: fontWeight)));
  }

  Widget txtarabfont(String txt,double size, FontWeight fontWeight, Color color) {
    return Text(txt,style: GoogleFonts.amiri(
        textStyle:
            TextStyle(
              height: 2.6,
              color: color, 
              fontSize: size, 
              fontWeight: fontWeight)));
  }


  
}

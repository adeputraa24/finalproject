import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:myquran/aboutusscreen.dart';
import 'package:myquran/auth/authfirebase.dart';
import 'package:myquran/controllers/surahverse_controller.dart';

import 'package:myquran/layout.dart';
import 'package:myquran/loginscreen.dart';
import 'package:myquran/popupmenu.dart';
import 'package:myquran/surahscreen.dart';

class homeScreen extends StatefulWidget {
  @override
  _homeScreenState createState() => _homeScreenState();
}

class _homeScreenState extends State<homeScreen> {
  final layout lyout = layout();

  final SurahVerseController svc = Get.find();

  @override
  Widget build(BuildContext context) {
    Query<Map<String, dynamic>> allSurah =
        FirebaseFirestore.instance.collection('allSurah').orderBy('nomer');

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: SizedBox(
          height: Get.height,
          width: Get.width,
          child: Column(
            children: [
              //Appbar container
              appBar(lyout: lyout),

              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 25),
                          child: lyout.txtmainfont('Assalamualaikum', 14,
                              FontWeight.w300, lyout.greycolor),
                        ),
                        const SizedBox(
                          height: 13,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 25),
                          child: lyout.txtmainfont(
                              FirebaseAuth.instance.currentUser!.displayName ??
                                  svc.displayName.value,
                              18,
                              FontWeight.w700,
                              Colors.black),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    lastReadwdgt(lyout: lyout, svc: svc),
                  ],
                ),
              ),
              const SizedBox(
                height: 25,
              ),

              //tab surah dan tafsir
              tabSurah(lyout: lyout),
              // ignore: sized_box_for_whitespace
              Container(
                  height: MediaQuery.of(context).size.height - 408.0,
                  child: TabBarView(
                      children: [tabsurah(allSurah), tabsurah(allSurah)])),
            ],
          ),
        ),
      ),
    );
  }
}

class tabSurah extends StatelessWidget {
  const tabSurah({
    Key? key,
    required this.lyout,
  }) : super(key: key);

  final layout lyout;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 25, top: 15, right: 25),
      child: TabBar(
          unselectedLabelColor: lyout.greycolor,
          indicatorColor: Color(0xFF6F40F2),
          labelColor: lyout.titlecolor,
          labelStyle: GoogleFonts.martelSans(
              color: lyout.titlecolor,
              fontSize: 14,
              fontWeight: FontWeight.w700),
          tabs: [
            Tab(child: Text('Surah')),
            Tab(child: Text('Tafsir')),
          ]),
    );
  }
}

class lastReadwdgt extends StatelessWidget {
  const lastReadwdgt({
    Key? key,
    required this.lyout,
    required this.svc,
  }) : super(key: key);

  final layout lyout;
  final SurahVerseController svc;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(surahScreen());
      },
      child: Container(
        margin: EdgeInsets.only(left: 25, right: 25),
        height: 116,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            //icon dan keterangan lanjut baca
            Padding(
              padding: const EdgeInsets.only(left: 17, top: 15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(
                    Icons.library_books,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  lyout.txtmainfont(
                      'Lanjut baca', 14, FontWeight.w600, Colors.white)
                ],
              ),
            ),
            // nama surah dan ayat terakhir dibaca
            Padding(
              padding: const EdgeInsets.only(left: 24, top: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Obx(() => lyout.txtmainfont('${svc.surahName.value}', 14,
                      FontWeight.w600, Colors.white)),
                  SizedBox(height: 5),
                  Obx(() => svc.surahName.value != ''
                      ? lyout.txtmainfont('Ayat No: ${svc.position.value + 1}',
                          14, FontWeight.w400, Colors.white)
                      : lyout.txtmainfont(
                          '', 14, FontWeight.w400, Colors.white)),
                ],
              ),
            )
          ]),
          Container(
            margin: EdgeInsets.only(right: 20, top: 10),
            child: Directionality(
                textDirection: TextDirection.rtl,
                child: Obx(() => lyout.txtarabfont('${svc.surahTitle.value}',
                    28, FontWeight.w700, Colors.white))),
          )
        ]),
        decoration: BoxDecoration(
            gradient: lyout.gradread,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.25),
                  offset: Offset(0, 10),
                  blurRadius: 10.0)
            ]),
      ),
    );
  }
}

class appBar extends StatelessWidget {
  const appBar({
    Key? key,
    required this.lyout,
  }) : super(key: key);

  final layout lyout;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 25, top: 49, right: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          lyout.txtmainfont('MyQuran', 18, FontWeight.w600, lyout.titlecolor),
          // SizedBox(width: 170),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(onPressed: () {}, icon: Icon(Icons.search)),
              // SizedBox(width: 5),
              PopupMenuButton<String>(
                onSelected: choiceAction,
                itemBuilder: (BuildContext context) {
                  return popup.choices.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}

Widget tabsurah(Query<Map<String, dynamic>> allSurah) {
  return StreamBuilder(
      stream: allSurah.snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
        if (streamSnapshot.connectionState == ConnectionState.waiting) {
          return Container(child: Center(child: CircularProgressIndicator()));
        }

        return ListView.builder(
            itemCount: streamSnapshot.data!.docs.length,
            itemBuilder: (ctx, index) {
              final String surahName =
                  streamSnapshot.data!.docs[index]['namaLatin'];

              final String surahTitle =
                  streamSnapshot.data!.docs[index]['nama'];
              final String surahPlace =
                  streamSnapshot.data!.docs[index]['tempatTurun'];
              final String jumlahAyat =
                  streamSnapshot.data!.docs[index]['jumlahAyat'].toString();

              return _surahContainer(
                  surahName, surahTitle, surahPlace, jumlahAyat, index);
            });
      });
}

Widget _surahContainer(String surahName, String surahTitle, String surahPlace,
    String numVerse, int nomerSurah) {
  final SurahVerseController svC = Get.find();
  // ignore: prefer_interpolation_to_compose_strings
  final String botsurName = surahPlace + ' : ' + numVerse + ' Ayat';
  return Padding(
    padding: const EdgeInsets.only(left: 25, right: 25),
    child: GestureDetector(
      onTap: () {
        if (nomerSurah == svC.noSurah.value) {
          Get.to(() => surahScreen());
        } else {
          svC.noSurah.value = nomerSurah;
          svC.position.value = 0;
          svC.surahName.value = surahName;
          svC.surahTitle.value = surahTitle;
          Get.to(() => surahScreen());
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 14, left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                layout()
                    .txtmainfont(surahName, 14, FontWeight.w700, Colors.black),
                // Spacer(),

                layout()
                    .txtmainfont(botsurName, 12, FontWeight.w300, Colors.black)
              ],
            ),
          ),
          // SizedBox(width: 120,),
          Container(
            margin: const EdgeInsets.only(right: 15, top: 12),
            child: Directionality(
                textDirection: TextDirection.rtl,
                child: layout().txtarabfont(
                    surahTitle, 24, FontWeight.w700, const Color(0xFF6F40F2))),
          )
        ],
      ),
    ),
  );
}

void choiceAction(String choice) async {
  if (choice == popup.AboutUs) {
    // ignore: avoid_print
    print('About Us');
    Get.to(() => aboutUsScreen());
  } else if (choice == popup.SignOut) {
    print('Sign Out');
    AuthService().signOut();

    Get.off(() => loginScreen());
  }
}

// List<Widget> rekomsurah = [
//   ElevatedButton(
//       style: elevbtnstyle,
//       onPressed: () {},
//       child:
//           layout().txtmainfont('Al-Kahf', 12, FontWeight.w600, Colors.black)),
//   SizedBox(
//     width: 15,
//   ),
//   ElevatedButton(
//       style: elevbtnstyle,
//       onPressed: () {},
//       child: layout().txtmainfont('Yasin', 12, FontWeight.w600, Colors.black)),
//   SizedBox(
//     width: 15,
//   ),
//   ElevatedButton(
//       style: elevbtnstyle,
//       onPressed: () {},
//       child:
//           layout().txtmainfont('Al-Mulk', 12, FontWeight.w600, Colors.black)),
//   SizedBox(
//     width: 15,
//   ),
//   ElevatedButton(
//       style: elevbtnstyle,
//       onPressed: () {},
//       child:
//           layout().txtmainfont('Ar-Rahman', 12, FontWeight.w600, Colors.black)),
//   SizedBox(
//     width: 15,
//   ),
//   ElevatedButton(
//       style: elevbtnstyle,
//       onPressed: () {},
//       child:
//           layout().txtmainfont('Al-Waqiah', 12, FontWeight.w600, Colors.black))
// ];

final ButtonStyle elevbtnstyle = ElevatedButton.styleFrom(
    onPrimary: Colors.grey[300], primary: Colors.white);

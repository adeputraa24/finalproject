import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:myquran/auth/authfirebase.dart';
import 'package:myquran/controllers/surah_controller.dart';
import 'package:myquran/homescreen.dart';
import 'package:myquran/layout.dart';
import 'package:myquran/registerscreen.dart';

class loginScreen extends StatefulWidget {
  @override
  _loginScreenState createState() => _loginScreenState();
}

class _loginScreenState extends State<loginScreen> {
  final _formKey = GlobalKey<FormState>();

  layout lyout = layout();

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _pwdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(left: 34, top: 66, right: 47),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  lyout.txtmainfont(
                      'Selamat Datang', 24, FontWeight.w600, lyout.titlecolor),
                  SizedBox(
                    height: 14,
                  ),
                  lyout.txtmainfont('Masuk untuk melanjutkan', 18,
                      FontWeight.w300, lyout.greycolor)
                ],
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                // padding: EdgeInsets.only(left:47-34),
                child: TextFormField(
                  controller: _emailController,
                  autocorrect: false,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Masukkan email anda.';
                    return null;
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.person_rounded,
                        color: Colors.black,
                      ),
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF1585D6))),
                      labelText: "Email "),
                ),
              ),
              SizedBox(
                height: 63,
              ),
              Container(
                // padding: EdgeInsets.only(left: 47-34),
                child: TextFormField(
                  controller: _pwdController,
                  autocorrect: false,
                  obscureText: true,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) return 'Masukkan password anda.';
                    return null;
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.lock,
                        color: Colors.black,
                      ),
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFF1585D6))),
                      labelText: "Password "),
                ),
              ),
              SizedBox(
                height: 69,
              ),
              Container(
                margin: EdgeInsets.only(left: 75, right: 75),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: lyout.btncolor,
                    borderRadius: BorderRadius.circular(25),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.3),
                          offset: Offset(0, 8),
                          blurRadius: 10.0)
                    ]),
                child: SizedBox(
                  width: 173,
                  child: ElevatedButton(
                    child: lyout.txtmainfont(
                        'Masuk', 18, FontWeight.w700, Colors.white),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()){
                        try {
                        Get.defaultDialog(title:'Login in',titleStyle: GoogleFonts.martelSans(),content: Center(child: CircularProgressIndicator(),));
                        await FirebaseAuth.instance
                            .signInWithEmailAndPassword(
                                email: _emailController.text,
                                password: _pwdController.text)
                            .then((value) {
                          Get.defaultDialog(title:'Login in',middleText: 'Login Berhasil!');
                          
                          Get.close(1);
                          _emailController.dispose();
                          _pwdController.dispose();
                          Get.off(() => homeScreen());
                          
                        });
                      } on FirebaseAuthException catch (e) {
                        if (e.code == 'user-not-found') {
                          Get.close(1);
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: lyout.txtmainfont('Akun tidak ditemukan.', 14, FontWeight.normal, Colors.white),
                            duration: Duration(seconds: 2),
                          ));
                        } else if (e.code == 'wrong-password') {
                          Get.close(1);
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: lyout.txtmainfont('Password salah.', 14, FontWeight.normal, Colors.white),
                            duration: Duration(seconds: 2),
                          ));
                        }
                        else{
                          Get.close(1);
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: lyout.txtmainfont('Silahkan coba lagi nanti.', 14, FontWeight.normal, Colors.white),
                            duration: Duration(seconds: 2),
                          ));
                        }
                      }
                      }
                      
                    },
                    style: elevbtnstyle,
                  ),
                ),
              ),
              SizedBox(height: 170),
              Container(
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 15),
                child: Column(
                  children: [
                    lyout.txtmainfont('Tidak memiliki akun?', 18, FontWeight.w200,
                        Colors.black),
                    SizedBox(
                      height: 13,
                    ),
                    TextButton(
                        onPressed: () {
                          Get.to(() => registerScreen());
                        },
                        child: lyout.txtmainfont(
                            'Daftar', 18, FontWeight.w600, lyout.titlecolor)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}

final ButtonStyle elevbtnstyle = ElevatedButton.styleFrom(
  shadowColor: Colors.black26,
  elevation: 10,
  onPrimary: Colors.grey[300],
  primary: layout().btncolor,
  minimumSize: Size(173, 54),
  // padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(25)),
  ),
);

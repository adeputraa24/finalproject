import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:myquran/controllers/surahverse_controller.dart';
import 'package:myquran/layout.dart';
import 'package:myquran/popupmenu.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class surahScreen extends StatelessWidget {
  layout lyout = layout();

  final SurahVerseController svc = Get.find();
  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();

  @override
  Widget build(BuildContext context) {
    final nomerSurah = svc.noSurah;
    final position = svc.position;

    Query<Map<String, dynamic>> detailSurah =
        FirebaseFirestore.instance.collection('detailSurah').orderBy('nomer');

    return StreamBuilder(
      stream: detailSurah.snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
        if (streamSnapshot.connectionState == ConnectionState.waiting) {
          return Scaffold(
            body: Center(child: CircularProgressIndicator()),
          );
        }
        if (streamSnapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text('Ada sesuatu yang salah silahkan coba lagi nanti.'),
            ),
          );
        }

        return Obx(() {
          final datasnapshot = streamSnapshot.data!.docs[nomerSurah.value];
          final String surahName = datasnapshot['surahName'];
          final String surahTitle = datasnapshot['surahTitle'];
          final String tempatSurah = datasnapshot['tempatSurah'];
          final int jumlahAyat = datasnapshot['jumlahAyat'];

          return WillPopScope(
            onWillPop: () {
              position.value =
                  itemPositionsListener.itemPositions.value.first.index;
              svc.surahName.value = surahName;
              svc.surahTitle.value = surahTitle;
              print(position.value);

              //trigger leaving and use own data
              Navigator.pop(context, false);
              //we need to return a future
              return Future.value(false);
            },
            child: Scaffold(
              body: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                      bgSurahScreen(
                          lyout: lyout,
                          surahTitle: surahTitle,
                          tempatSurah: tempatSurah,
                          jumlahAyat: jumlahAyat),

                      //list view box putih ayat
                      listContent(
                        lyout: lyout,
                        snapshot: datasnapshot,
                        itemPositionsListener: itemPositionsListener,
                        itemScrollController: itemScrollController,
                        position: position.value,
                      ),

                      // app bar surah screen
                      appBarSurah(
                        lyout: lyout,
                        surahName: surahName,
                        position: position,
                        itemPositionsListener: itemPositionsListener,
                        itemScrollController: itemScrollController,
                      )
                    ],
                  )),
            ),
          );
        });
      },
    );
  }
}

class appBarSurah extends StatelessWidget {
  const appBarSurah({
    Key? key,
    required this.lyout,
    required this.surahName,
    required this.itemPositionsListener,
    required this.itemScrollController,
    required this.position,
  }) : super(key: key);

  final layout lyout;
  final String surahName;
  final ItemPositionsListener itemPositionsListener;
  final position;
  final ItemScrollController itemScrollController;

  @override
  Widget build(BuildContext context) {
    final SurahVerseController svc = Get.find();
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 49),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        IconButton(
            onPressed: () {
              Get.back();
              position.value =
                  itemPositionsListener.itemPositions.value.first.index;
            },
            icon: Icon(
              Icons.chevron_left,
              size: 36,
              color: Colors.white,
            )),
        lyout.txtmainfont(surahName, 18, FontWeight.w600, Colors.white),

        // DropdownButtonHideUnderline(
        //   child: DropdownButton(
        //       dropdownColor: Color(0xFF3A7BD5),
        //       value: svc.noSurah.value,
        //       onChanged: (int? value) {
        //         svc.noSurah.value = value!;
        //         svc.position.value = 0;
        //       },
        //       items: svc.listSurahName.map((String nameSurah) {
        //         return DropdownMenuItem<int>(
        //           child: lyout.txtmainfont(
        //               nameSurah, 18, FontWeight.w600, Colors.white),
        //           value: svc.listSurahName.indexOf(nameSurah),
        //         );
        //       }).toList()),
        // ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            // IconButton(
            //     onPressed: () {},
            //     icon: Icon(
            //       Icons.search,
            //       size: 28,
            //       color: Colors.white,
            //     )),
            PopupMenuButton<String>(
              onSelected: choiceAction,
              icon: Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
              itemBuilder: (BuildContext context) {
                return popup.choicesSurah.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            )
          ],
        )
      ]),
    );
  }

  void choiceAction(String choice) async {
    final TextEditingController _ayatController = TextEditingController();
    if (choice == popup.LompatAyat) {
      Get.defaultDialog(
        title: 'Lompat ayat',
        confirmTextColor: Colors.white,
        content: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            controller: _ayatController,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF1585D6)))),
          ),
        ),
        confirm: ElevatedButton(
            onPressed: () {
              print(_ayatController.text);
              itemScrollController.jumpTo(
                  index: int.parse(_ayatController.text) - 1);
              Get.back();
            },
            child: Text("Confirm")),

        // onCancel: () => Get.back(),
      );

      // Get.to(() => aboutUsScreen());
    }
  }
}

class listContent extends StatelessWidget {
  const listContent({
    Key? key,
    required this.lyout,
    required this.snapshot,
    required this.itemScrollController,
    required this.itemPositionsListener,
    required this.position,
  }) : super(key: key);

  final layout lyout;
  final QueryDocumentSnapshot snapshot;
  final ItemScrollController itemScrollController;
  final ItemPositionsListener itemPositionsListener;
  final position;

  @override
  Widget build(BuildContext context) {
    final SurahVerseController svc = Get.find();
    return ListView(
      controller: svc.scrollController,
      padding: EdgeInsets.only(top: 220),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 100),
          child: Divider(
            thickness: 3,
            color: lyout.bgayatcolor,
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: const EdgeInsets.only(top: 25,bottom: 10),
            height: MediaQuery.of(context).size.height - 100,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: lyout.bgcolor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(50),
                    topRight: Radius.circular(50))),
            child: NotificationListener<ScrollNotification>(
              onNotification: (scrollNotification){
                if(scrollNotification is ScrollUpdateNotification){
                  svc.scrollController.animateTo(svc.scrollController.offset + 100, duration: Duration(milliseconds: 400), curve: Curves.linear);
                  
                }
                else if(scrollNotification is ScrollStartNotification){
                  svc.scrollController.animateTo(svc.scrollController.offset - 140, duration: Duration(milliseconds: 400), curve: Curves.linear);
                }
                return false;

              },
              child: ScrollablePositionedList.builder(
                itemCount: snapshot['arti'].length,
                initialScrollIndex: position,
                itemScrollController: itemScrollController,
                itemPositionsListener: itemPositionsListener,
                itemBuilder: (context, i) {
                  final arti = snapshot['arti'][i].toString();
                  final tempNo = i + 1;
                  final nomer = tempNo.toString();
            
                  final ayat = snapshot['ayat'][i].toString();
            
                  return Column(
                    children: [
                      boxAyat(
                        lyout: lyout,
                        ayat: ayat,
                        nomer: nomer,
                      ),
                      boxArti(lyout: lyout, arti: arti)
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class bgSurahScreen extends StatelessWidget {
  const bgSurahScreen({
    Key? key,
    required this.lyout,
    required this.surahTitle,
    required this.tempatSurah,
    required this.jumlahAyat,
  }) : super(key: key);

  final layout lyout;
  final String surahTitle;
  final String tempatSurah;
  final int jumlahAyat;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(gradient: lyout.gradayat),
      child: Padding(
        padding: const EdgeInsets.only(top: 110, left: 43, right: 43),
        child: Column(children: [
          lyout.txtmainfont(surahTitle, 24, FontWeight.w600, Colors.white),
          const Padding(
            padding: EdgeInsets.only(top: 10),
            child: Divider(
              color: Colors.white,
              thickness: 0.5,
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: lyout.txtmainfont('$tempatSurah : $jumlahAyat Ayat', 12,
                FontWeight.w700, Colors.white),
          )
        ]),
      ),
    );
  }
}

class boxArti extends StatelessWidget {
  const boxArti({
    Key? key,
    required this.lyout,
    required this.arti,
  }) : super(key: key);

  final layout lyout;
  final String arti;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 35, right: 35),
      width: MediaQuery.of(context).size.width,
      child: lyout.txtmainfont(arti, 14, FontWeight.w400, Colors.black),
    );
  }
}

class boxAyat extends StatelessWidget {
  const boxAyat({
    Key? key,
    required this.lyout,
    required this.ayat,
    required this.nomer,
  }) : super(key: key);

  final layout lyout;
  final String ayat;
  final String nomer;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 25, right: 25),
      decoration: BoxDecoration(
          color: lyout.bgayatcolor, borderRadius: BorderRadius.circular(20)),
      child: Container(
          // alignment: Alignment.centerRight,
          // width: MediaQuery.of(context).size.width - 25,
          padding:
              const EdgeInsets.only(top: 37, bottom: 37, left: 15, right: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 24,
                height: 24,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: lyout.titlecolor,
                ),
                child: Center(
                    child: Text(
                  nomer,
                  style: const TextStyle(fontSize: 12, color: Colors.white),
                )),
              ),
              // ignore: sized_box_for_whitespace
              Container(
                // margin: EdgeInsets.only(right: 15),
                width: Get.width - 120,
                child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: lyout.txtarabfont(
                        ayat, 22, FontWeight.w500, Colors.black)),
              )
            ],
          )),
    );
  }
}

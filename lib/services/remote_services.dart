import 'dart:async';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:retry/retry.dart';
import 'package:myquran/model/detailSurah_model.dart';
import 'package:myquran/model/listSurah_model.dart';

import 'package:myquran/model/surahheroku.dart';
import 'package:myquran/model/surahverse.dart';

// ignore: avoid_classes_with_only_static_members
class RemoteServices {
  static http.Client client = http.Client();

  static Future<List<ListSurah>> fetchAllSurah() async {
    const String url = 'https://equran.id/api/surat';

    final response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final jsonString = response.body;
      return listSurahFromJson(jsonString);
    } else {
      //show error message
      return [];
    }
  }

  static Future fetchSurahHeroku() async {
    // String url = 'https://raw.githubusercontent.com/penggguna/QuranJSON/master/quran.json';
    // String url = 'http://adeputra.live/api/quran.json';
    const String url = 'https://myquranapi.herokuapp.com/surah';
    final response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final jsonString = response.body;
      return surahHerokuFromJson(jsonString);
    } else {
      //show error message
      return [];
    }
  }

  static Future<Surahverse?> fetchSurahVerse(String noSurah) async {
    // String url = 'https://raw.githubusercontent.com/penggguna/QuranJSON/master/quran.json';
    // String url = 'http://adeputra.live/api/quran.json';
    final String url = 'https://myquranapi.herokuapp.com/surah/$noSurah';

    // var response = await client.get(Uri.parse(url));
    final response = await retry(
        () => client.get(Uri.parse(url)).timeout(const Duration(seconds: 15)),
        retryIf: (e) => e is SocketException || e is TimeoutException);
    if (response.statusCode == 200) {
      final jsonString = response.body;

      return surahverseFromJson(jsonString);
    }
    else {
      //show error message
      return null;
    }
  }

  static Future fetchDetailSurah(String noSurah) async {
    // String url = 'https://raw.githubusercontent.com/penggguna/QuranJSON/master/quran.json';
    // String url = 'http://adeputra.live/api/quran.json';
    final String url = 'https://equran.id/api/surat/$noSurah';

    final response = await retry(
        () => client.get(Uri.parse(url)).timeout(const Duration(seconds: 5)),
        retryIf: (e) => e is SocketException || e is TimeoutException);
    // var response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final jsonString = response.body;

      return detailSurahFromJson(jsonString);
    } else {
      //show error message
      return [];
    }
  }
}

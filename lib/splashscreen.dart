import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:myquran/layout.dart';
import 'package:myquran/loginscreen.dart';

class splashScreen extends StatelessWidget {
  layout lyout = layout();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 25, top: 66, right: 25),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                DelayedDisplay(
                  delay: Duration(seconds: 1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      lyout.txtmainfont('MyQuran App',
                              32, FontWeight.w800, lyout.titlecolor),
                      SizedBox(height: 16),
                      lyout.txtmainfont('Your friend to jannah',18, FontWeight.w400, lyout.greycolor),
                      
                    ],
                  ),
                ),
                SizedBox(
                  height: 51,
                ),
                Stack(
                  clipBehavior: Clip.none,
                  children: [
                    DelayedDisplay(
                      delay: Duration(seconds: 1),
                      child: Container(
                        padding: EdgeInsets.only(left: 54, right: 54),
                        height: MediaQuery.of(context).size.height - 350,
                        child: Image.asset('assets/img/quranf.png'),
                        decoration: BoxDecoration(
                          color: lyout.greencolor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                    ),
                    Positioned(
                        top: MediaQuery.of(context).size.height - 380,
                        left: MediaQuery.of(context).size.width - 320,
                        right: MediaQuery.of(context).size.width -320,
                        child: DelayedDisplay(
                          delay: Duration(milliseconds: 1800),
                          child: Container(
                            decoration: BoxDecoration(
                                color: lyout.btncolor,
                                borderRadius: BorderRadius.circular(25),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black.withOpacity(0.3),
                                      offset: Offset(0, 8),
                                      blurRadius: 10.0)
                                ]),
                            child: ElevatedButton(
                              child: lyout.txtmainfont('Mulai',
                                    18, FontWeight.w700, Colors.white),
                              
                              onPressed: () {
                                Get.off(() => loginScreen());
                              },
                              style: elevbtnstyle,
                            ),
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

final ButtonStyle elevbtnstyle = ElevatedButton.styleFrom(
  shadowColor: Colors.black26,
  elevation: 10,
  onPrimary: Colors.grey[300],
  primary: layout().btncolor,
  minimumSize: Size(135, 54),
  // padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(25)),
  ),
);

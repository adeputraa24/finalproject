import 'package:get/state_manager.dart';
import 'package:myquran/model/listSurah_model.dart';
import 'package:myquran/model/surahheroku.dart' as SurahHeroku;


import 'package:myquran/services/remote_services.dart';

class SurahController extends GetxController {
  var isLoading = true.obs;
  

  // ignore: deprecated_member_use
  var allSurahList = <ListSurah>[].obs;
  var surahListHeroku = <SurahHeroku.Surah>[].obs;
  

  
  @override
  void onInit() {
    // fetchSurahHeroku();
    // fetchSurahVerse1();
    

    fetchAllSurah();

    super.onInit();
  }

  

  // Future<void> fetchSurahHeroku() async {
  //   try {
  //     isLoading(true);
  //     var surahHero = <SurahHeroku.Surah>[];
  //     surahHero.add(await RemoteServices.fetchSurahHeroku());
  //     if (surahHero != []) {
  //       surahListHeroku.value = surahHero;
  //     }
  //   } finally {
  //     isLoading(false);
  //   }
  // }

  Future<void> fetchAllSurah() async{
    try{
      isLoading(true);
      final List<ListSurah> allSurah = await RemoteServices.fetchAllSurah();
      if (allSurah != []){
        allSurahList.value = allSurah;
      }
    }
    finally{
      isLoading(false);
    }
  }

}
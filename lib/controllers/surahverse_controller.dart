
import 'package:flutter/cupertino.dart';
import 'package:get/state_manager.dart';
// import 'package:myquran/model/detailSurah_model.dart';
// import 'package:myquran/model/surahverse.dart';
// import 'package:myquran/services/remote_services.dart';


class SurahVerseController extends GetxController {
  var isLoading = true.obs;
  // var allSurah = <List<Surahverse>>[];

  // ignore: deprecated_member_use

  // var surahverse = <Surahverse>[].obs;
  // var detailSurah = <DetailSurah>[].obs;
  var noSurah = 0.obs;
  var surahName = ''.obs;
  var surahTitle = ''.obs;
  var displayName = ''.obs;
  var position = 0.obs;
  var listSurahName = <String>[];
  final scrollController = ScrollController();

  // List listSurahVerse = [].obs;

  @override
  void onInit() {
    // fetchSurahHeroku();
    // fetchSurahVerse();
    // fetchdetailSurah();
    

    super.onInit();
  }

  // Future<void> fetchSurahVerse() async {
  //   // try {
  //   isLoading(true);
  //   final surahVerse = <Surahverse>[];
  //   final List<Future<Surahverse?>> listFetchSurah = List.generate(114, (int i) {
  //     final int nomer = i + 1;
  //     return RemoteServices.fetchSurahVerse(nomer.toString());
  //   });

  //   await Future.wait(listFetchSurah).then((value) {
  //     for (final response in value) {
  //       surahVerse.add(response!);
  //       surahverse.value = surahVerse;
  //     }
  //   }).whenComplete(() {
  //     isLoading(false);
  //   });
  // }

  // Future<void> fetchdetailSurah() async {
  //   // try {
  //   isLoading(true);
  //   var listDetailSurah = <DetailSurah>[];

  //   var listFetchSurah = List.generate(114, (int i) {
  //     int nomer = i + 1;
  //     return RemoteServices.fetchDetailSurah(nomer.toString());
  //   });

  //   await Future.wait(listFetchSurah).then((value) {
  //     for (var response in value) {
  //       listDetailSurah.add(response);
  //       detailSurah.value = listDetailSurah;
  //     }
  //   }).whenComplete(() {
  //     isLoading(false);
  //   });
  // }
}

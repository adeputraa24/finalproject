import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myquran/controllers/surah_controller.dart';
import 'package:myquran/controllers/surahverse_controller.dart';
import 'package:myquran/layout.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class testScreen extends StatefulWidget {
  // final SurahController sc = Get.find();
  // final SurahVerseController svc = Get.find();

  @override
  _testScreenState createState() => _testScreenState();
}

class _testScreenState extends State<testScreen> {
  @override
  Widget build(BuildContext context) {
    Query<Map<String, dynamic>> allSurah =
        FirebaseFirestore.instance.collection('detailSurah').orderBy('nomer');
    final lyout = layout();
    var _value = 0;
    final _listItems = [
      'Kata pendek',
      'Kata agak panjang',
      'Kata yang lumayan panjang'
    ];

    return CustomScrollView(
      slivers: [
        SliverAppBar(
          // floating: true,
          pinned: true,
          // snap: true,
          expandedHeight: 200,
          leading: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Icon(Icons.chevron_left),
          ),
          title: lyout.txtmainfont(
              'surahTitle', 18, FontWeight.w600, Colors.white),
          centerTitle: true,
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Icon(Icons.more_vert),
            )
          ],
          flexibleSpace: FlexibleSpaceBar(
            titlePadding: EdgeInsets.only(bottom: 10),
            title: SingleChildScrollView(
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                lyout.txtmainfont(
                    'surahTitle', 14, FontWeight.w600, Colors.white),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Divider(
                    color: Colors.white,
                    thickness: 0.5,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 10),
                  child: lyout.txtmainfont('tempatSurah : jumlahAyat Ayat', 10,
                      FontWeight.w700, Colors.white),
                )
              ]),
            ),
            centerTitle: true,
          ),
        ),
        SliverList(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
          return Card(
            margin: EdgeInsets.all(50),
            child: Text('Teks $index'),
            elevation: 5,
          );
        }, childCount: 20))
      ],
    );

    // return Scaffold(

    //     // body: StreamBuilder(
    //     //   stream: allSurah.snapshots(),
    //     //   builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot){
    //     //     return ListView.builder(
    //     //       itemCount: streamSnapshot.data!.docs[0]['arti'].length,
    //     //       itemBuilder: (ctx, index) =>
    //     //             Text(streamSnapshot.data!.docs[0]['arti'][index].toString()),
    //     //             );
    //     //   },
    //     //   ),
    //     );
  }
}

findSurah(List surah, String surahName) {
  // Return list of people matching the condition
  final foundSurah = surah.where((element) => element.name == surahName);

  if (foundSurah.isNotEmpty) {
    return foundSurah.first;
  }
}


// return Obx(() {
//       if (svc.isLoading.value) {
//         return const Scaffold(body: Center(child: CircularProgressIndicator()));
//       } else {
//         var detailSurah = svc.surahverse;

//         detailSurah.forEach((element) {
//           List ayat = [];
//           List arti = [];
//           element.data.verses.forEach((ayah) {ayat.add(ayah.text.arab); });
//           element.data.verses.forEach((trans) {arti.add(trans.translation.id); });
//           detailSurahdb
//               .add({
//                 'nomer': element.data.number,
//                 "surahName": element.data.name.transliteration.id,
//                 "surahTitle": element.data.name.short,
//                 "tempatSurah": element.data.revelation.id,
//                 "jumlahAyat":
//                     element.data.numberOfVerses,
//                 "ayat": ayat,
//                 "arti": arti,
//               })
//               .then((value) => print('surah ditambahkan'))
//               .catchError((error) => print('gagal : $error'));
//         });

//         return Scaffold(
//           body: Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 ElevatedButton(
//                     onPressed: () {
//                       Get.defaultDialog(middleText: 'dialog screen');
//                     },
//                     child: const Text('Dialog')),
               
//               ],
//             ),
//           ),
         
//         );
//       }
//     });
import 'package:flutter/material.dart';
import 'package:myquran/layout.dart';
import 'package:myquran/popupmenu.dart';

class aboutUsScreen extends StatelessWidget {
  layout lyout = layout();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //Appbar container
          Container(
            padding: EdgeInsets.only(left: 10, top: 49, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.chevron_left,
                      size: 28,
                      color: Colors.black,
                    )),
                lyout.txtmainfont(
                    'About Us', 18, FontWeight.w600, lyout.titlecolor),
                Row(
                  children: [
                    IconButton(onPressed: () {}, icon: Icon(Icons.search)),
                    PopupMenuButton<String>(
                      onSelected: choiceAction,
                      itemBuilder: (BuildContext context) {
                        return popup.choices.map((String choice) {
                          return PopupMenuItem<String>(
                            value: choice,
                            child: Text(choice),
                          );
                        }).toList();
                      },
                    )
                  ],
                ),
              ],
            ),
          ),

          //about me box
          Container(
            margin: EdgeInsets.all(25),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 450,
            decoration: BoxDecoration(
                color: lyout.bgayatcolor,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.25),
                      offset: Offset(0, 10),
                      blurRadius: 10.0)
                ]),
            child: Padding(
              padding: const EdgeInsets.only(top:24.0,bottom: 59),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ClipRRect(
                    child: Image.asset(
                      'assets/img/me.png',
                      width: 131,
                      height: 131,
                    ),
                  ),
                  lyout.txtmainfont(
                      'Ade Putra', 18, FontWeight.w700, Colors.black),
                  lyout.txtmainfont('Hi, Im the developer of this app', 14,
                      FontWeight.w700, Colors.black)
                ],
              ),
            ),
          ),

          //know more about me
          Container(
            margin: EdgeInsets.all(25),
            height: 150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                lyout.txtmainfont('Know more about me on:', 18, FontWeight.w400, Colors.black),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset('assets/img/linkedin.png',width: 48,height: 48,),
                    Image.asset('assets/img/Instagram.png',width: 48,height: 48,),
                    Image.asset('assets/img/github.png',width: 48,height: 48,)
                  ],
                )
              ],
            ),
          )

        ],
      ),
    );
  }
}

void choiceAction(String choice) {
  if (choice == popup.AboutUs) {
    print('About Us');
  } else if (choice == popup.SignOut) {
    print('Sign Out');
  }
}
